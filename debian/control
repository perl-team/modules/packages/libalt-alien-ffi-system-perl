Source: libalt-alien-ffi-system-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: libffi-dev <!nocheck>,
                     libtest2-suite-perl <!nocheck>,
                     perl,
                     pkg-config <!nocheck> | libpkgconfig-perl <!nocheck>
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libalt-alien-ffi-system-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libalt-alien-ffi-system-perl.git
Homepage: https://metacpan.org/release/Alt-Alien-FFI-System

Package: libalt-alien-ffi-system-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libffi-dev,
         pkg-config | libpkgconfig-perl
Conflicts: libalien-ffi-perl
Provides: libalien-ffi-perl
Description: simplified alternative to Alien::FFI that uses system libffi
 Alien::FFI makes libffi available to other Perl distributions.
 .
 Alt::Alien::FFI::System provides an alternative implementation of Alien::FFI
 that is geared toward system integrators when libffi is provided by the
 operating system. It will NOT attempt to download or install libffi, contrary
 to the original Alien::FFI.
 .
 DEPRECATED: As of version 0.69, FFI::Platypus will use pkg-config and the
 system libffi if it is already installed, and skip the install of Alien::FFI.
 Thus, this module is now redundant. It may be removed from the CPAN and from
 Debian.
